﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class Note
    {
        public static int count = 0;
        public int id = 0;
        public string firstname;
        public string familyName;
        public string otherName;
        public string nomber;
        public string country;
        public DateTime dateForBithday;
        public string organizate;
        public string position;
        public string other;

        public Note (string firstname, string familyName, string nomber, string country)
        {
            this.id = ++count;
            this.firstname = firstname;
            this.familyName = familyName;
            otherName = "";
            this.nomber = nomber;
            this.country = country;
            dateForBithday = new DateTime(0001, 01, 01);
            organizate = "";
            position = "";
            other = "";
        }
    }
}
