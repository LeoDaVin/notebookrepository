﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    class NoteBook
    {
        public static Dictionary<int, Note> Book = new Dictionary<int, Note>();

        public static void NewNote()
        {
            Console.Clear();
            Console.WriteLine("Для создания новой записи необходимо ввести обязательные параметры");

            Console.Write("\nВведите имя: ");
            string firstname = Console.ReadLine();
            CheckValid(ref firstname);

            Console.Write("\nВведите фамилию: ");
            string familyName = Console.ReadLine();
            CheckValid(ref familyName);

            Console.Write("\nВведите номер телефона: ");
            string nomber = Console.ReadLine();
            CheckNomber(ref nomber);

            Console.Write("\nВведите страну: ");
            string country = Console.ReadLine();
            CheckValid(ref country);


            Note newNote = new Note(firstname, familyName, nomber, country);
            Book.Add(newNote.id, newNote);

            bool switchOff = true;
            while (switchOff)
            {
                {//Вывод данных текущей записи
                    Console.Clear();
                    Console.WriteLine("Вы создали следующего пользователя:");

                    Console.WriteLine("\nФамилия: {0}", newNote.familyName);
                    Console.WriteLine("Имя: {0}", newNote.firstname);
                    Console.WriteLine("Отчество: {0}", newNote.otherName);
                    Console.WriteLine("Номер телефона: {0}", newNote.nomber);
                    Console.WriteLine("Страна: {0}", newNote.country);
                    Console.WriteLine("Дата рождения: {0}", newNote.dateForBithday.ToShortDateString());
                    Console.WriteLine("Организация: {0}", newNote.organizate);
                    Console.WriteLine("Должность: {0}", newNote.position);
                    Console.WriteLine("Прочие заметки: {0}", newNote.other);

                    Console.WriteLine("\nВы можете добавить следующие необязательные поля текущей записи:\n");
                }//Вывод данных текущей записи

                { // Меню выбора
                    Console.WriteLine("1. Отчество;");
                    Console.WriteLine("2. Дата рождения;");
                    Console.WriteLine("3. Организация;");
                    Console.WriteLine("4. Должность;");
                    Console.WriteLine("5. Прочие заметки.");

                    Console.WriteLine("\nЕсли вы НЕ хотите добавлять поля к текущей записи введите \"f\"\n");
                } // Меню выбора

                Console.Write("Введите символ пункта меню: ");
                string str = Console.ReadLine();

                switch (str)
                {
                    case "1":
                        Console.Write("\nВведите отчество: ");
                        string otherName = Console.ReadLine();
                        CheckValid(ref otherName);
                        newNote.otherName = otherName;
                        break;
                    case "2":
                        Console.Write("\nВведите Дату рождения в формате \"01.02.1990\": ");
                        string dB = Console.ReadLine();
                        CheckDate(ref dB);
                        int d, m, y;
                        d = int.Parse(dB.Split('.')[0]);
                        m = int.Parse(dB.Split('.')[1]);
                        y = int.Parse(dB.Split('.')[2]);
                        newNote.dateForBithday = new DateTime(y, m, d);
                        break;
                    case "3":
                        Console.Write("\nВведите организацию: ");
                        string organizate = Console.ReadLine();
                        CheckValid(ref organizate);
                        newNote.organizate = organizate;
                        break;
                    case "4":
                        Console.Write("\nВведите должность: ");
                        string position = Console.ReadLine();
                        CheckValid(ref position);
                        newNote.position = position;
                        break;
                    case "5":
                        Console.Write("\nВведите прочие заметки: ");
                        string other = Console.ReadLine();
                        newNote.other = other;
                        break;
                    case "f":
                        Console.Clear();
                        switchOff = false;
                        break;
                    default:
                        //Console.Clear();
                        Console.WriteLine("\nВы ввели неверный пункт меню. Попробуйте выбрать еще раз.\n");
                        break;
                } //Переключатель пунктов меню 
            }

        }
        public static void DelNote()
        {
            string switchOff;
            bool validB = true;
            Console.Clear();

            while (validB)
            {
                while (true)
                {
                    Console.WriteLine("Введите номер записи, которую хотите удалить.");
                    Console.WriteLine("\nЕсли вы не знаете номер записи, можете вывести на дисплей\n" +
                                      "существующие записи введя \"d\"");
                    Console.WriteLine("\nДля возрата в предыдущее меню введите \"q\".\n");
                    Console.Write("Введите значение: ");
                    switchOff = Console.ReadLine();

                    if ((switchOff != "d" &&
                        switchOff != "q" &&
                        (switchOff.Length > Book.Count.ToString().Length && Book.Count > 0) &&
                        int.TryParse(switchOff, out int id)) ||
                        switchOff == "")
                    {
                        Console.Clear();
                        Console.WriteLine("\nВы ввели недействительное значение, введите номер записи\n" +
                            "или один из пунктов, преложенных ниже.\n");
                    }
                    else
                        break;
                }

                switch (switchOff)
                {
                    case "d":
                        LookNoteAll();
                        break;
                    case "q":
                        validB = false;
                        Console.Clear();
                        break;
                    default:
                        int.TryParse(switchOff, out int id);
                        if (!Book.ContainsKey(id))
                        {
                            Console.WriteLine("\nУказанный номер отсутствует в NoteBook.\n");
                            Console.WriteLine("Нажмите любую клавишу чтобы продлолжить.\n");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                        {
                            Book.Remove(id);
                            Console.WriteLine("Удалена запись с номером: {0}.\n", id);
                            Console.WriteLine("Нажмите любую клавишу чтобы продлолжить.\n");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                }
            }

        }
        public static void DifNote()
        {
            string switchOff;
            bool validB = true;
            Console.Clear();

            while (validB)
            {
                while (true)
                {
                    Console.WriteLine("Введите номер записи, которую хотите изменить.");
                    Console.WriteLine("\nЕсли вы не знаете номер записи, можете вывести на дисплей\n" +
                                      "существующие записи введя \"d\"");
                    Console.WriteLine("\nДля возрата в предыдущее меню введите \"q\".\n");
                    Console.Write("Введите значение: ");
                    switchOff = Console.ReadLine();

                    if ((switchOff != "d" &&
                        switchOff != "q" &&
                        (switchOff.Length > Book.Count.ToString().Length && Book.Count > 0) &&
                        int.TryParse(switchOff, out int id)) ||
                        switchOff == "")
                    {
                        Console.Clear();
                        Console.WriteLine("\nВы ввели недействительное значение, введите номер записи\n" +
                            "или один из пунктов, преложенных ниже.\n");
                    }
                    else
                        break;
                }

                switch (switchOff)
                {
                    case "d":
                        LookNoteAll();
                        break;
                    case "q":
                        validB = false;
                        Console.Clear();
                        break;
                    default:
                        int.TryParse(switchOff, out int id);
                        if (!Book.ContainsKey(id))
                        {
                            Console.WriteLine("\nУказанный номер отсутствует в NoteBook.\n");
                            Console.WriteLine("Нажмите любую клавишу чтобы продлолжить.\n");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                            Difine(id);
                        break;
                }
            }
        }
        public static void Difine(int id)
        {
            foreach (var item in Book)
            {
                if (item.Key == id)
                {
                    string switchOff;
                    bool validB = true;
                    // Console.Clear();

                    while (validB)
                    {
                        Console.WriteLine("\nТекущая запись:");
                        Console.WriteLine("\n1. Фамилия: {0}", item.Value.familyName);
                        Console.WriteLine("2. Имя: {0}", item.Value.firstname);
                        Console.WriteLine("3. Отчество: {0}", item.Value.otherName);
                        Console.WriteLine("4. Номер телефона: {0}", item.Value.nomber);
                        Console.WriteLine("5. Страна: {0}", item.Value.country);
                        Console.WriteLine("6. Дата рождения: {0}", item.Value.dateForBithday.ToShortDateString());
                        Console.WriteLine("7. Организация: {0}", item.Value.organizate);
                        Console.WriteLine("8. Должность: {0}", item.Value.position);
                        Console.WriteLine("9. Прочие заметки: {0}", item.Value.other);

                        Console.WriteLine("\nВведите номер элемента, который хотите изменить.");
                        Console.WriteLine("\nДля возрата в предыдущее меню введите \"q\".\n");

                        Console.Write("Введите значение: ");
                        switchOff = Console.ReadLine();

                        switch (switchOff)
                        {
                            case "1":
                                Console.Write("\nВведите фамилию: ");
                                string familyName = Console.ReadLine();
                                CheckValid(ref familyName);
                                item.Value.familyName = familyName;
                                break;
                            case "2":
                                Console.Write("\nВведите имя: ");
                                string firstname = Console.ReadLine();
                                CheckValid(ref firstname);
                                item.Value.firstname = firstname;
                                break;
                            case "3":
                                Console.Write("\nВведите отчество: ");
                                string otherName = Console.ReadLine();
                                CheckValid(ref otherName);
                                item.Value.otherName = otherName;
                                break;
                            case "4":
                                Console.Write("\nВведите номер телефона: ");
                                string nomber = Console.ReadLine();
                                CheckNomber(ref nomber);
                                item.Value.otherName = nomber;
                                break;
                            case "5":
                                Console.Write("\nВведите страну: ");
                                string country = Console.ReadLine();
                                CheckValid(ref country);
                                item.Value.country = country;
                                break;
                            case "6":
                                Console.Write("\nВведите Дату рождения в формате \"01.02.1990\": ");
                                string dB = Console.ReadLine();
                                CheckDate(ref dB);
                                int d, m, y;
                                d = int.Parse(dB.Split('.')[0]);
                                m = int.Parse(dB.Split('.')[1]);
                                y = int.Parse(dB.Split('.')[2]);
                                item.Value.dateForBithday = new DateTime(y, m, d);
                                break;
                            case "7":
                                Console.Write("\nВведите организацию: ");
                                string organizate = Console.ReadLine();
                                CheckValid(ref organizate);
                                item.Value.organizate = organizate;
                                break;
                            case "8":
                                Console.Write("\nВведите должность: ");
                                string position = Console.ReadLine();
                                CheckValid(ref position);
                                item.Value.position = position;
                                break;
                            case "9":
                                Console.Write("\nВведите прочие заметки: ");
                                string other = Console.ReadLine();
                                item.Value.other = other;
                                break;
                            case "q":
                                Console.Clear();
                                validB = false;
                                break;
                            default:
                                Console.WriteLine("\nВы ввели неверный пункт меню. Попробуйте выбрать еще раз.\n");
                                break;
                        } //Переключатель пунктов меню 
                        Console.Clear();

                    }

                }

            }
        }

        public static void LookNoteN()
        {
            string switchOff;
            bool validB = true;
            Console.Clear();

            while (validB)
            {
                while (true)
                {
                    Console.WriteLine("Введите номер записи, которую хотите просмотреть.");
                    Console.WriteLine("\nЕсли вы не знаете номер записи, можете вывести на дисплей\n" +
                                      "существующие записи введя \"d\"");
                    Console.WriteLine("\nДля возрата в предыдущее меню введите \"q\".\n");
                    Console.Write("Введите значение: ");
                    switchOff = Console.ReadLine();

                    if ((switchOff != "d" &&
                        switchOff != "q" &&
                        (switchOff.Length > Book.Count.ToString().Length && Book.Count > 0) &&
                        int.TryParse(switchOff, out int id)) ||
                        switchOff == "")
                    {
                        Console.Clear();
                        Console.WriteLine("\nВы ввели недействительное значение, введите номер записи\n" +
                            "или один из пунктов, преложенных ниже.\n");
                    }
                    else
                        break;
                }

                switch (switchOff)
                {
                    case "d":
                        LookNoteAll();
                        break;
                    case "q":
                        validB = false;
                        Console.Clear();
                        break;
                    default:
                        int.TryParse(switchOff, out int id);
                        if (!Book.ContainsKey(id))
                        {
                            Console.WriteLine("\nУказанный номер отсутствует в NoteBook.\n");
                            Console.WriteLine("Нажмите любую клавишу чтобы продлолжить.\n");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                            foreach (var item in Book)
                            {
                                if (item.Key == id)
                                {
                                    Console.WriteLine("\nТекущая запись:");
                                    Console.WriteLine("\n1. Фамилия: {0}", item.Value.familyName);
                                    Console.WriteLine("2. Имя: {0}", item.Value.firstname);
                                    Console.WriteLine("3. Отчество: {0}", item.Value.otherName);
                                    Console.WriteLine("4. Номер телефона: {0}", item.Value.nomber);
                                    Console.WriteLine("5. Страна: {0}", item.Value.country);
                                    Console.WriteLine("6. Дата рождения: {0}", item.Value.dateForBithday.ToShortDateString());
                                    Console.WriteLine("7. Организация: {0}", item.Value.organizate);
                                    Console.WriteLine("8. Должность: {0}", item.Value.position);
                                    Console.WriteLine("9. Прочие заметки: {0}", item.Value.other);

                                    Console.WriteLine("\nНажмите любую клавишу чтобы продлолжить.\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                }
                            }
                        break;
                }


            }
        }
        public static void LookNoteAll()
        {
            Console.Clear();
            Console.WriteLine("Содержащиеся в NoteBook записи: ");

            if (Book.Count == 0)
            {
                Console.WriteLine("\nЗаписей пока нет.");
            }
            else
                foreach (var item in Book)
                {
                    Console.WriteLine("\nНомер записи: " + item.Value.id);
                    Console.WriteLine("Фамилия: " + item.Value.familyName);
                    Console.WriteLine("Имя: " + item.Value.firstname);
                    Console.WriteLine("Номер телефона: " + item.Value.nomber);
                }

            Console.WriteLine("Для возврата в предыдущее меню нажмите любую клавишу.");
            Console.ReadKey();
            Console.Clear();
        }



        public static void CheckValid(ref string val)
        {
            string validS;
            bool validB = true;

            while (validB)
            {
                while (true) // Проверка на длинну значения
                {
                    validB = false;

                    if (val.Length > 10 || val.Length < 1)
                    {
                        Console.Clear();
                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВы ввели пустое значение или состоящее больше чем из 10 символов.\n");
                        Console.WriteLine("Желаете оставить введенное значение без изменений?");
                        Console.Write("\nВведите \"f\", если хотите продолжить без изменений.\n" +
                                      "Чтобы изменить значение нажмите Enter: ");
                        validS = Console.ReadLine();
                        if (validS != "f")
                        {
                            Console.Write("\nВведите измененное значение: ");
                            val = Console.ReadLine();
                        }
                        else break;
                    }
                    else
                    {
                        break;
                    }
                } // Проверка на длинну значения

                while (true) // Проверка на невалидные символы
                {
                    char[] letters = {
                        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
                        'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                        'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
                    };

                    int count = 0;
                    for (int i = 0; i < val.Length; i++)
                    {
                        if (!letters.Contains(Char.ToLower(val[i])))
                        {
                            //Console.WriteLine($"У тебя тут \"{val[i]}\", кажись это нифига не буква");
                            count++;
                        }
                    }

                    if (count > 0)
                    {
                        Console.Clear();
                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВы ввели значение, содержащее не только буквы, но и другие символы.\n");
                        Console.WriteLine("Желаете оставить введенное значение без изменений?");
                        Console.Write("\nВведите \"f\", если хотите продолжить без изменений.\n" +
                                     "Чтобы изменить значение нажмите Enter: ");
                        validS = Console.ReadLine();
                        if (validS != "f")
                        {
                            Console.Write("\nВведите измененное значение: ");
                            val = Console.ReadLine();

                            if (val.Length > 10 || val.Length < 1)
                                validB = true;
                        }
                        else
                            break;
                    }
                    else break;
                } // Проверка на невалидные символы
            }
        }
        public static void CheckNomber(ref string val)
        {
            bool validB = true;

            while (validB)
            {
                while (true) //Проверка на длинну номера
                {
                    validB = false;

                    if (val.Length > 18 || val.Length < 1)
                    {
                        Console.Clear();
                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВведеное значение не может быть пустым или больше 18 символов.");
                        Console.Write("\nВведите корректное значение: ");
                        val = Console.ReadLine();
                    }
                    else
                        break;
                }           //Проверка на длинну номера

                while (true) // Проверка на невалидные символы
                {
                    char[] num = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                    int count = 0;

                    for (int i = 0; i < val.Length; i++)
                    {
                        if (!num.Contains(val[i]))
                        {
                            //Console.WriteLine($"У тебя тут \"{val[i]}\", кажись это нифига не цифра");
                            count++;
                        }
                    }

                    if (count > 0)
                    {
                        Console.Clear();
                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВы ввели значение, содержащее не только цифры, но и другие символы.\n");
                        Console.WriteLine("Вам необходимо ввести значение, состоящее исключительно из цифр");

                        Console.Write("\nВведите корректное значение: ");
                        val = Console.ReadLine();

                        if (val.Length > 18 || val.Length < 1)
                            validB = true;
                    }
                    else
                        break;
                } // Проверка на невалидные символы
            }
        }
        public static void CheckDate(ref string val)
        {
            bool validB = true;
            bool validForm = false;

            while (validB)
            {
                while (validForm) //Проверка формата
                {
                    int d, m, y;
                    d = int.Parse(val.Split('.')[0]);
                    m = int.Parse(val.Split('.')[1]);
                    y = int.Parse(val.Split('.')[2]);

                    //Console.WriteLine("{0}-{1}-{2}", d, m, y);

                    if (d == 0 || m == 0 || y == 0 || m > 12 || d > 31 ||
                        ((m == 2 && (y % 400 == 0 || (y % 100 != 0 && y % 4 == 0)) && d > 29) ||
                        (m == 2 && ((y % 400 != 0 && y % 100 == 0) || y % 4 != 0) && d > 28) ||
                        ((m == 4 || m == 6 || m == 9 || m == 11) && d > 30)))
                    {
                        Console.Clear();
                        Console.WriteLine("Не соблюден формат даты.\n");

                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВведеное значение должно быть в формате \"dd.mm.yyyy\"");
                        Console.WriteLine("Значения даты не могут выходить за формат стандартного календаря.");
                        Console.Write("\nВведите корректное значение: ");
                        val = Console.ReadLine();
                    }
                    else
                    {
                        validB = false;
                        break;
                    }

                } //Проверка формата

                while (true) // Проверка на невалидные символы
                {
                    char[] num = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.' };
                    int count = 0;

                    for (int i = 0; i < val.Length; i++)
                    {
                        if (!num.Contains(val[i]))
                        {
                            //Console.WriteLine($"У тебя тут \"{val[i]}\", кажись этим дату не нарисовать");
                            count++;
                        }
                    }

                    if (count > 0 || val.Length != 10)
                    {
                        validB = true;
                        Console.Clear();
                        Console.WriteLine($"Ваше значение: {val}");
                        Console.WriteLine("\nВы ввели значение, содержащее не верные символы.");
                        Console.WriteLine("Дата может состоять только из цифр и разделителя \".\".\n");
                        Console.WriteLine("Вам необходимо ввести верное значение, в формате \"01.01.1900\".");

                        Console.Write("\nВведите корректное значение: ");
                        val = Console.ReadLine();
                    }
                    else
                    {
                        validForm = true;
                        break;
                    }
                } // Проверка на невалидные символы

            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в прогамму NoteBook!");

            bool switchOff = true;
            while (switchOff)
            {
                { // Меню приветствия
                    Console.WriteLine("\nВыберите, что необходимо сделать далее.\n");
                    Console.WriteLine("1. Создать запись;");
                    Console.WriteLine("2. Редактировать запись;");
                    Console.WriteLine("3. Удалить запись;");
                    Console.WriteLine("4. Просмотреть созданную запись;");
                    Console.WriteLine("5. Посмотреть основную информацию по всем созданным записям.");
                    Console.WriteLine("\nЕсли вы НЕ хотите работать с NoteBook введите \"q\"\n");
                } // Меню приветствия

                Console.Write("Введите символ пункта меню: ");
                string str = Console.ReadLine();

                if (str.Length > 1)
                {
                    while (str.Length > 1)
                    {
                        Console.Write("\nВы ввели неверный пункт меню. Введите повторно: ");
                        str = Console.ReadLine();
                    }
                }
                switch (str)
                {
                    case "1":
                        NewNote();
                        break;
                    case "2":
                        DifNote();
                        break;
                    case "3":
                        DelNote();
                        break;
                    case "4":
                        LookNoteN();
                        break;
                    case "5":
                        LookNoteAll();
                        break;
                    case "q":
                        switchOff = false;
                        Console.Clear();
                        Console.WriteLine("Благодарим за использование NoteBook!");
                        Console.WriteLine("Нажмите любую клавишу для выхода.");
                        Console.ReadKey();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\nВы ввели неверный пункт меню. Попробуйте выбрать еще раз.\n");
                        break;
                } //Переключатель пунктов меню

            }
        }
    }

}

